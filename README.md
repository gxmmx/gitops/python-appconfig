# python-appconfig

This is a testing repository for publishing to PyPI.

# Local development

```shell
python3.11 -m venv .venv
source .venv/bin/activate
pip install --upgrade pip
pip install build twine

```
