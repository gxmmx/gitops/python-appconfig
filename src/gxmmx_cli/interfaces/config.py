from ..internal.variables import CommandFactory, VariableFactory, ArgumentFactory, PositionalFactory, UsageFactory
from ..internal.handlers import ConfigHandler

class ConfigInterface:
    """
    Config Interface

    Interface for handling configuration of app parameters before start.
    """

    # ----------------------------
    # Basic App Configuration
    # ----------------------------

    @staticmethod
    def title(title:str) -> None:
        """
        Sets app title

        Only used for display.
        Default if not set: to sys.argv[0]
        """
        ConfigHandler.set_title(title)

    @staticmethod
    def name(name:str) -> None:
        """
        Sets app name

        Used by various aspects of CLI.
        Default if not set: to sys.argv[0]
        """
        ConfigHandler.set_name(name)

    @staticmethod
    def description(description:str) -> None:
        """
        Sets app description

        Used for documentation and help.
        Default if not set: CLI generated.
        """
        ConfigHandler.set_description(description)

    @staticmethod
    def version(version:str) -> None:
        """
        Sets app version

        Often set by __version__.
        Must be valid semantic version string.
        Default if not set: "0.0.0"
        """
        ConfigHandler.set_version(version)

    # ----------------------------
    # Passed Argument configuration
    # ----------------------------

    @staticmethod
    def use_arguments(arguments:list) -> None:
        """
        Passed list of arguments to parse.

        If not set, will use sys.argv[1:]
        """
        ConfigHandler.use_arguments(arguments)

    # ----------------------------
    # Logging Configuration
    # ----------------------------

    @staticmethod
    def loglevel(level:str) -> None:
        """
        Sets initial log level

        Default if not set: info
        """
        ConfigHandler.set_loglevel(level)

    @staticmethod
    def log_enable(enable:bool=True) -> None:
        """
        Enables log to file

        Default if not set: False
        """
        ConfigHandler.set_log_enabled(enable)

    @staticmethod
    def log_file(path:str) -> None:
        """
        Log file path

        Default if not set: cwd/appname.log
        """
        ConfigHandler.set_log_file(path)

    @staticmethod
    def log_format_file(format:str) -> None:
        """
        Log format for logfile

        Default if not set: 'timestamp - level - msg'
        """
        ConfigHandler.set_log_format_file(format)

    @staticmethod
    def log_format_stdout(format:str) -> None:
        """
        Log format for stdout console

        Default if not set: 'msg'
        """
        ConfigHandler.set_log_format_stdout(format)

    @staticmethod
    def log_format_stderr(format:str) -> None:
        """
        Log format for stderr console

        Default if not set: 'level: msg'
        """
        ConfigHandler.set_log_format_stderr(format)

    @staticmethod
    def log_format_debug(format:str) -> None:
        """
        Log format debug

        Default if not set: 'level: msg'
        """
        ConfigHandler.set_log_format_debug(format)

    # ----------------------------
    # Locking Configuration
    # ----------------------------

    @staticmethod
    def lock_enable(enable:bool=True) -> None:
        """
        Enable file locking

        Default if not set: False
        """
        ConfigHandler.set_lock_enabled(enable)

    @staticmethod
    def lock_file(path:str) -> None:
        """
        Lock file path

        Default if not set: cwd/appname.lock
        """
        ConfigHandler.set_lock_file(path)

    # ----------------------------
    # Config file Configuration
    # ----------------------------

    @staticmethod
    def add_config_file(path:str) -> None:
        """
        Adds yml config file source

        Config files are looked up in the order they are added.
        Default if not set: cwd/appname.yml
        """
        ConfigHandler.add_config_source(path)

    # ----------------------------
    # Variable Configuration
    # ----------------------------

    @staticmethod
    def add_command(name:str, help:str, callable:bool=False) -> None:
        """
        Adds callable command to cli

        Variables and arguments can then be assigned to multiple commands.
        By default all leaf commands are callable,
        but if you want an itermediate command to be callable it must be specified.
        """
        CommandFactory.add_command(name, help, callable)

    @staticmethod
    def add_variable(name:str, help:str, type:str|None=None, fmt:str|None=None, env:str|None=None, cnf:str|None=None, default:str|None=None, cmd:str|None=None, grp:str|None=None) -> None:
        """
        Adds variable usable by program.

        Can be validated against a specific type and format.
        Can be sourced from environment variables or config file entries, as well as a default value.
        Can be assigned to commands (cmds) so they are only required if specific commands are called.
        """
        VariableFactory.variable(name=name, help=help, type=type, fmt=fmt, env=env, cnf=cnf, default=default, cmd=cmd, grp=grp, remove=False)

    @staticmethod
    def add_argument(name:str, flg:str, help:str, value:any=None, help_value:str|None=None, type:str|None=None, fmt:str|None=None, env:str|None=None, cnf:str|None=None, default:str|None=None, cmd:str|None=None, grp:str|None=None) -> None:
        """
        Adds callable argument to cli.

        An argument uses a variable.
        Multiple arguments can be assigned to the same variable.
        Variable will be created if it does not exist.
        If a value is not passed, it is expected on the command line.
        If a value is passed the argument becomes a flag only, accepting no value.
        """
        var = VariableFactory.variable(name=name, help=help, type=type, fmt=fmt, env=env, cnf=cnf, default=default, cmd=cmd, grp=grp, remove=False)
        ArgumentFactory.add_argument(var=var, flg=flg, help=help, help_value=help_value, value=value)

    @staticmethod
    def add_positional(name:str, help:str, recurring:bool=False, help_value:str|None=None, type:str|None=None, fmt:str|None=None, env:str|None=None, cnf:str|None=None, default:str|None=None, cmd:str|None=None) -> None:
        """
        Adds positional parameter to cli.

        If num is 0, it can take an arbitrary amount of values.
        Only one positional parameter can be added to each command.
        """
        var = VariableFactory.variable(name=name, help=help, type=type, fmt=fmt, env=env, cnf=cnf, default=default, cmd=cmd, grp=None, remove=False)
        PositionalFactory.add_positional(var=var, help=help, recurring=recurring, help_value=help_value)

    @staticmethod
    def add_usage(example:str, cmd:str|None=None) -> None:
        """
        Adds usage example to cli usage

        Can be linked to specifice commands only 
        """
        UsageFactory.add_usage(example, cmd)
