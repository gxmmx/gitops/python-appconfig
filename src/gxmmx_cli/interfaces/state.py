from ..internal.handlers import ConfigHandler, LogHandler, LockHandler, ExitHandler
from ..internal.variables import VariableHandler
from ..internal.usage import UsageHandler


class StateInterface:
    """
    State Interface

    Interface for starting and exiting the cli application.
    Available methods are: start,exit
    """

    started = False

    @classmethod
    def start(cls) -> None:
        """
        Start CLI

        Call after configuration to begin execution.
        """
        if cls.started:
            return None
        cls.started = True

        ConfigHandler.parse()
        VariableHandler.add_default_arguments()
        VariableHandler.parse()
        LogHandler.parse()
        UsageHandler.parse()

        ExitHandler.register()
        LockHandler.register()

    @classmethod
    def exit(cls, code:int=0, usage:bool=False) -> None:
        """
        Exit CLI

        Exits cli with defined exit code.
        Can take a usage bool to show usage on exit.
        """

        if usage:  # pragma: no cover
            UsageHandler.show_help()
        ExitHandler.exit(code)
