from ..internal.handlers import LogHandler

class LogInterface:
    """
    Log Interface.

    Interface for logging messages.
    Available methods are: debug,info,warning,critical
    to log respective messages.
    """

    @staticmethod
    def debug(msg:str) -> None:
        """
        Log debug message

        Logs to stdout.
        """
        LogHandler.log_debug(msg)

    @staticmethod
    def info(msg:str) -> None:
        """
        Log standard message

        Logs to stdout.
        """
        LogHandler.log_info(msg)

    @staticmethod
    def warning(msg:str) -> None:
        """
        Log warning message

        Logs to stderr.
        """
        LogHandler.log_warning(msg)
    
    @staticmethod
    def critical(msg:str) -> None:
        """
        Log critical message

        Logs to stderr.
        """
        LogHandler.log_critical(msg)
