
from ..internal.variables import CommandFactory, VariableFactory, Command

class DataInterface:
    """
    Data Interface.

    Interface for fetching data set by CLI.
    Available methods are: variables,variable,command
    """

    @staticmethod
    def variables() -> dict[str,any]:
        """
        Return all variables set by CLI
        """
        return VariableFactory.get_variable_dict()

    @staticmethod
    def variable(name:str) -> any:
        """
        Return variable by name

        Returns None if name is not found.
        """
        return VariableFactory.get_variable_value(name)

    @staticmethod
    def command() -> str|None:
        """
        Return current command name

        Returns ':' separated command name or None
        if not running any command.
        """
        cmd:Command|None = CommandFactory.get_current()
        if cmd is None:  # pragma: no cover
            return None
        return cmd.name
