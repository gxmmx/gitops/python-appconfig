from .interfaces.config import ConfigInterface
from .interfaces.data import DataInterface
from .interfaces.log import LogInterface
from .interfaces.state import StateInterface

# ------------------------------------------------------------------------------
# App Factory
# ------------------------------------------------------------------------------

class _AppFactory(type):
    _instance = None
    _instance_cls = None
    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(_AppFactory, cls).__call__(*args, **kwargs)
        return cls._instance

# ------------------------------------------------------------------------------
# AppCLI
# ------------------------------------------------------------------------------

class AppCLI(metaclass=_AppFactory):
    """
    GxMMx App CLI interface
    
    Handles basic functionality of programs.
    """

    def __init__(self):
        self.config = ConfigInterface()
        self.data = DataInterface()
        self.log = LogInterface()

    def start(self):
        """
        Start cli
        Parses arguments and variable sources.
        Initializes logging and functionality.
        """
        StateInterface.start()

    def exit(self, code:int=0, usage:bool=False) -> None:
        """
        Stop cli
        Exit with code.
        """
        StateInterface.exit(code, usage)
