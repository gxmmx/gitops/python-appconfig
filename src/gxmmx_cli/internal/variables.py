from __future__ import annotations

from functools import reduce
import os
import yaml

from ..internal.errors import AppCLIVariableDefinitionError
from ..internal.handlers import LogHandler, ConfigHandler
from ..internal.validator import ValidatorFactory

# ------------------------------------------------------------------------------
# Commands
# ------------------------------------------------------------------------------

class Command:
    def __init__(self, name:str, help:str, callable:bool=False) -> None:
        # Info
        if not name:
            raise AppCLIVariableDefinitionError("Command can not contain empty name")
        self.name = name
        self.help = help
        self.callable = callable

        # Alt names
        self.name_full = name.replace(':',' ').strip()
        self.name_list = name.split(':')
        name_parts = name.rsplit(':',1)

        if len(name_parts) > 1:
            self.name_parent, self.name_leaf = name_parts[:2]
        else:
            self.name_parent = ""
            self.name_leaf = name

    def __eq__(self, other:Command) -> bool:
        return self.name == other.name

class CommandFactory:
    _instance_list: list[Command] = []
    _instance_dict: dict[str, Command] = {}
    _current = None

    @classmethod
    def add_command(cls,name:str,help:str,callable:bool=False) -> Command:
        if name in cls._instance_dict:
            raise AppCLIVariableDefinitionError(f"Command '{name}' already declared")
        instance = Command(name,help,callable)
        if instance.name_parent and instance.name_parent not in cls._instance_dict:
            raise AppCLIVariableDefinitionError(f"Command '{name}' has undefined parent command '{instance.name_parent}'")
        cls._instance_dict[name] = instance
        cls._instance_list.append(instance)
        return instance

    @classmethod
    def get_command(cls, name:str) -> Command|None:
        return cls._instance_dict.get(name, None)

    @classmethod
    def get_children(cls, cmd:Command|None=None) -> list[Command]:
        parent = cmd.name if cmd is not None else ""
        return [instance for instance in cls._instance_list if instance.name_parent == parent]

    @classmethod
    def set_current(cls, cmd:Command|None=None) -> None:
        if not cls.get_children(cmd):
            if isinstance(cmd, Command):
                cmd.callable = True
        cls._current = cmd

    @classmethod
    def get_current(cls) -> Command|None:
        return cls._current


# ------------------------------------------------------------------------------
# Usage
# ------------------------------------------------------------------------------

class Usage:
    def __init__(self, example:str, cmd:str|None=None) -> None:
        # Info
        self.example = example

        # Commands
        self.cmds:list[Command] = []
        if cmd is not None and cmd:
            for cmdstr in cmd.split(','):
                command = CommandFactory.get_command(cmdstr)
                if command is None:
                    raise AppCLIVariableDefinitionError(f"Usage example references undefined command '{cmdstr}'")
                self.cmds.append(command)

    def __eq__(self, other:Usage) -> bool:
        return self.example == other.example


class UsageFactory:
    _instance_list: list[Usage] = []

    @classmethod
    def add_usage(cls,example:str,cmd:str|None=None) -> Usage:
        instance = Usage(example,cmd)
        cls._instance_list.append(instance)
        return instance

    @classmethod
    def get_usages(cls, cmd:Command|None=None) -> list[Usage]:
        usages = []
        for usage in cls._instance_list:
            if cmd is None and not usage.cmds:
                usages.append(usage)
            elif cmd is not None and cmd in usage.cmds:
                usages.append(usage)
        return usages


# ------------------------------------------------------------------------------
# Variables
# ------------------------------------------------------------------------------

class Variable:
    def __init__(self, name:str, help:str, type:str|None=None, fmt:str|None=None, env:str|None=None, cnf:str|None=None, default:any|None=None,cmd:str|None=None,grp:str|None=None,remove:bool=False) -> None:

        # Info
        if not name:
            raise AppCLIVariableDefinitionError("Variable can not contain empty name")
        self.name = name
        self.help = help
        self.group = grp
        self.remove = remove

        # Value
        self.default = default
        self.default_placeholder = None
        self.value = None

        # Validation (type, fmt)
        self.validator = None
        if type is not None:
            self.validator = ValidatorFactory.create(name, type, fmt)

        # Config parameters
        self.cnfs:list[CnfVar] = []
        if cnf is not None and cnf:
            for cnfstr in cnf.split(','):
                cnfvar = CnfVarFactory.add_cnfvar(cnfstr, self)
                self.cnfs.append(cnfvar)

        # Environment variables
        self.envs:list[EnvVar] = []
        if env is not None and env:
            for envstr in env.split(','):
                envvar = EnvVarFactory.add_envvar(envstr, self)
                self.envs.append(envvar)

        # Commands
        self.cmds:list[Command] = []
        if cmd is not None and cmd:
            for cmdstr in cmd.split(','):
                command = CommandFactory.get_command(cmdstr)
                if command is None:
                    raise AppCLIVariableDefinitionError(f"Variable '{name}' references undefined command '{cmdstr}'")
                self.cmds.append(command)

        self.args:list[Argument] = []
        self.poss:list[Positional] = []

    def add_argument(self, arg:Argument):
        if len(self.poss):
            raise AppCLIVariableDefinitionError(f"Variable '{self.name}' already assigned to positional argument")
        self.args.append(arg)

    def add_positional(self, pos:Positional):
        if len(self.args):
            raise AppCLIVariableDefinitionError(f"Variable '{self.name}' already assigned to argument")
        if len(self.poss):
            raise AppCLIVariableDefinitionError(f"Variable '{self.name}' already assigned to positional argument")
        self.poss.append(pos)

    def __eq__(self, other:Variable) -> bool:
        return self.name == other.name

    def set_sources(self) -> None:
        for pos in self.poss:
            if pos.value is not None:
                self.value = pos.value
                return None
        for arg in self.args:
            if arg.called and arg.value is not None:
                self.value = arg.value
                return None
        for env in self.envs:
            if env.value is not None:
                self.value = env.value
                self.default = env.value
                self.default_placeholder = env.name
                return None
        for cnf in self.cnfs:
            if cnf.value is not None:
                self.value = cnf.value
                self.default = cnf.value
                self.default_placeholder = cnf.name
                return None
        if self.default is not None:
            self.value = self.default


class VariableFactory:
    _instance_list: list[Variable] = []
    _instance_dict: dict[str, Variable] = {}

    @classmethod
    def variable(cls,name:str,help:str,type:str|None=None,fmt:str|None=None,env:str|None=None,cnf:str|None=None,default:any|None=None,cmd:str|None=None,grp:str|None=None, remove:bool=False) -> Variable:
        if name in cls._instance_dict:
            return cls._instance_dict[name]
        instance = Variable(name,help,type,fmt,env,cnf,default,cmd,grp,remove)
        cls._instance_dict[name] = instance
        cls._instance_list.append(instance)
        return instance

    @classmethod
    def parse_all(cls):
        for var in cls._instance_list:
            var.set_sources()
            if var.value is not None and var.validator is not None:
                if not var.validator.validate(var.value):
                    LogHandler.log_argerr(f"{var.validator.error()}")

    @classmethod
    def check_missing(cls, cmd:Command|None) -> None:
        for var in cls._instance_list:
            if len(var.cmds) == 0 and var.value is None:
                if not var.poss:
                    LogHandler.log_argerr(f"Required variable '{var.name}' missing")
            elif cmd is not None and cmd in var.cmds and var.value is None:
                if not var.poss:
                    LogHandler.log_argerr(f"Required variable '{var.name}' missing")

    @classmethod
    def get_variable_value(cls, name:str, get_removed:bool=False) -> any:
        if name not in cls._instance_dict:
            return None
        var = cls._instance_dict[name]
        if var.remove and not get_removed:
            return None
        return var.value

    @classmethod
    def get_variable_dict(cls) -> dict[str,any]:
        all_vars = {}
        for var in cls._instance_list:
            if var.remove:
                continue
            all_vars[var.name] = var.value
        return all_vars



# ------------------------------------------------------------------------------
# Arguments
# ------------------------------------------------------------------------------

class Argument:
    def __init__(self, var:Variable, help:str, flg:str, help_value:str|None=None, value:any=None) -> None:
        self.var = var
        self.help = help
        self.value = value
        self.called = False

        # Flags
        if not flg:
            raise AppCLIVariableDefinitionError(f"Argument for '{self.var.name}' has empty flag declaration")
        long = []
        short = []
        for flag in flg.split(','):
            flag = flag[2:] if flag.startswith('--') else flag
            flag = flag[1:] if flag.startswith('-') else flag
            if len(flag) > 1:
                long.append(f"--{flag}")
            else:
                short.append(f"-{flag}")
        self.flags = long
        self.flags.extend(short)

        if len(self.flags) > len(set(self.flags)):
            raise AppCLIVariableDefinitionError(f"Argument for '{self.var.name}' has duplicate flag declaration")

        # Help value
        if self.value is None:
            if help_value is not None and help_value:
                self.help_value = help_value
            elif self.var.validator is not None:
                self.help_value = f"<{self.var.validator.type_name}>"
            else:
                self.help_value = "<value>"
        else:
            self.help_value = None
        
        if self.help_value is not None:
            self.flag_for_print = f"{self.flags[0]} {self.help_value}"
            self.flags_for_print = ', '.join([f"{i} {self.help_value}" for i in self.flags])
        else:
            self.flag_for_print = f"{self.flags[0]}"
            self.flags_for_print = ', '.join(self.flags)

    def __eq__(self, other:Argument):
        return bool(set(self.flags) & set(other.flags))




class ArgumentFactory:
    _instance_list: list[Argument] = []

    @classmethod
    def add_argument(cls,var:Variable,help:str,flg:str,help_value:str|None=None,value:any=None) -> None:
        instance = Argument(var, help, flg, help_value, value)
        if instance in cls._instance_list:
            raise AppCLIVariableDefinitionError(f"Argument for '{var.name}' has a flag declaration already in use by another argument")
        var.add_argument(instance)
        cls._instance_list.append(instance)

    @classmethod
    def get_arguments(cls, cmd:Command|None=None) -> list[Argument]:
        args = []
        for arg in cls._instance_list:
            if not arg.var.cmds:
                args.append(arg)
            elif cmd is not None:
                if cmd in arg.var.cmds:
                    args.append(arg)
        return args



# ------------------------------------------------------------------------------
# Positionals
# ------------------------------------------------------------------------------

class Positional:
    # def __init__(self, var:Variable, help:str, num:int=1, optional:bool=False, help_value:str|None=None) -> None:
    def __init__(self, var:Variable, help:str, recurring:bool=False, help_value:str|None=None) -> None:
        self.var = var
        self.help = help
        self.recurring = recurring

        self.value = None

        if help_value is not None and help_value:
            self.help_value = help_value
        elif self.var.validator is not None:
            self.help_value = f"<{self.var.validator.type_name}>"
        else:
            self.help_value = "<value>"
        if self.recurring:
            self.help_value = f"{self.help_value}..."


class PositionalFactory:
    _instance_list: list[Positional] = []

    @classmethod
    def add_positional(cls, var:Variable, help:str, recurring:bool=False, help_value:str|None=None) -> Positional:
        instance = Positional(var, help, recurring, help_value)

        if len(instance.var.cmds) == 0:
            for pos in cls._instance_list:
                if len(pos.var.cmds) == 0:
                    if pos.recurring:
                        raise AppCLIVariableDefinitionError("Only one recurring positional can be declared without a command")
        else:
            for pos in cls._instance_list:
                if len(pos.var.cmds) > 0:
                    p_names = [p.name for p in pos.var.cmds]
                    i_names = [i.name for i in instance.var.cmds]
                    if any(name in i_names for name in p_names):
                        if pos.recurring:
                            raise AppCLIVariableDefinitionError("Only one recurring can be declared per command")
        var.add_positional(instance)
        cls._instance_list.append(instance)

    @classmethod
    def get_positionals(cls, cmd:Command|None=None) -> list[Positional]:
        positionals:list[Positional] = []
        for pos in cls._instance_list:
            if cmd is None:
                if not pos.var.cmds:
                    positionals.append(pos)
            else:
                if cmd in pos.var.cmds:
                    positionals.append(pos)
        return positionals


# ------------------------------------------------------------------------------
# Environment variables
# ------------------------------------------------------------------------------

class EnvVar:
    def __init__(self, name:str, var:Variable) -> None:
        self.name = name
        self.var = var
        self.value = None

    def set_value(self) -> None:
        self.value = os.getenv(self.name, None)

    def __eq__(self, other:EnvVar) -> bool:
        return self.name == other.name

class EnvVarFactory:
    _instance_dict: dict[str, EnvVar] = {}

    @classmethod
    def add_envvar(cls, name:str, var:Variable) -> EnvVar:
        if name in cls._instance_dict:
            raise AppCLIVariableDefinitionError(f"Environment variable '{name}' already declared")
        instance = EnvVar(name, var)
        cls._instance_dict[name] = instance
        return instance

    @classmethod
    def get_envvar(cls, name:str) -> EnvVar|None:
        return cls._instance_dict.get(name, None)

    @classmethod
    def parse_all(cls) -> None:
        envvar: EnvVar
        for envvar in cls._instance_dict.values():
            envvar.set_value()


# ------------------------------------------------------------------------------
# Config variables
# ------------------------------------------------------------------------------

class CnfVar:
    def __init__(self, name:str, var:Variable) -> None:
        self.name = name
        self.var = var
        self.value = None

    def set_value(self, value:any) -> None:
        self.value = value

    def __eq__(self, other:CnfVar) -> bool:
        return self.name == other.name

class CnfVarFactory:
    _instance_dict: dict[str, CnfVar] = {}
    _config_sources: list[dict] = []

    @classmethod
    def add_cnfvar(cls, name:str, var:Variable) -> CnfVar:
        if name in cls._instance_dict:
            raise AppCLIVariableDefinitionError(f"Config variable key '{name}' already declared")
        instance = CnfVar(name, var)
        cls._instance_dict[name] = instance
        return instance

    @classmethod
    def get_cnfvar(cls, name:str) -> CnfVar|None:
        return cls._instance_dict.get(name, None)

    @classmethod
    def load_source(cls, path:str) -> None:
        try:
            with open(path, 'r') as file:
                cls._config_sources.append(yaml.safe_load(file))
            LogHandler.log_debug(f"Config file loaded: {path}")
        except FileNotFoundError:
            LogHandler.log_debug(f"Config file not found (skipping): {path}")
        except:
            LogHandler.log_debug(f"Config file could not be loaded (skipping): {path}")

    @classmethod
    def parse_all(cls) -> None:
        cnfvar: CnfVar
        for cnfvar in cls._instance_dict.values():
            keys = cnfvar.name.split('.')
            for source in cls._config_sources:
                val = reduce(lambda d, key: d.get(key, None) if isinstance(d, dict) else None, keys, source)
                if val is not None:
                    cnfvar.set_value(val)
                    break


# ------------------------------------------------------------------------------
# Variable Handler
# ------------------------------------------------------------------------------

class VariableHandler:

    @staticmethod
    def _parse_sources() -> None:
        # Parse environment sources
        EnvVarFactory.parse_all()

        # Add config source from env
        env_config_source = EnvVarFactory.get_envvar(f"{ConfigHandler.env_slug}_CONFIG")
        if env_config_source.value is not None:
            ConfigHandler.config_sources.insert(0,env_config_source.value)

        # Load config sources
        for cnfsource in ConfigHandler.config_sources:
            CnfVarFactory.load_source(cnfsource)
        # Parse config sources
        CnfVarFactory.parse_all()

    @staticmethod
    def _extract_command(args:list[str]) -> tuple[Command,list[str]]:
        current_cmd = []
        for cmd in CommandFactory._instance_list:
            if args[:len(cmd.name_list)] == cmd.name_list:
                if len(cmd.name_list) > len(current_cmd):
                    current_cmd = cmd.name_list.copy()
        args = args[len(current_cmd):]
        cmd_found = CommandFactory.get_command(':'.join(current_cmd)) if current_cmd else None
        CommandFactory.set_current(cmd_found)
        return cmd_found, args

    @staticmethod
    def _expand_shorts(args:list[str]) -> list[str]:
        expanded = []
        for arg in args:
            if arg.startswith('-') and not arg.startswith('--') and len(arg) > 2:
                for char in arg[1:]:
                    expanded.append(f'-{char}')
            else:
                expanded.append(arg)
        return expanded

    @staticmethod
    def _expand_equals(args:list[str]) -> list[str]:
        expanded = []
        for arg in args:
            if arg.startswith('--') and '=' in arg:
                expanded.extend(arg.split('=', 1))
            else:
                expanded.append(arg)
        return expanded

    @staticmethod
    def _parse_arguments(command:Command, args:list[str]) -> list[str]|None:
        args_left:list[str] = []
        args_avail = ArgumentFactory.get_arguments(command)
        flags_used = set()
        vars_used = set()

        arg_is_val = False
        for i, arg_passed in enumerate(args):
            if arg_is_val:
                arg_is_val = False
                continue
            arg_found = False
            for arg_avail in args_avail:
                if arg_passed in arg_avail.flags:
                    if arg_passed in flags_used:
                        LogHandler.log_argerr(f"Argument '{arg_passed}' has multiple declarations")
                        return None
                    flags_used.add(arg_passed)
                    if arg_avail.var.name in vars_used:
                        LogHandler.log_argerr(f"Argument '{arg_passed}' can not be called with mutual argument")
                        return None
                    vars_used.add(arg_avail.var.name)
                    arg_found = True
                    arg_avail.called = True
                    if arg_avail.value is None:
                        arg_is_val = True
                        if i + 1 < len(args):
                            arg_avail.value = args[i + 1]
                        else:
                            LogHandler.log_argerr(f"Argument '{arg_passed}' missing required value")
                            return None
            if not arg_found:
                args_left.append(arg_passed)

        for arg_left in args_left:
            # if arg_left.startswith('-'):
            if arg_left.startswith('-') and not arg_left == '--':
                LogHandler.log_argerr(f"Unknown argument '{arg_left}'")
                return None
        return args_left

    @staticmethod
    def _parse_positional(command:Command, args:list[str]|None) -> None:
        if args is None:
            return None
        posi = PositionalFactory.get_positionals(command)
        for pos in posi:
            if pos.recurring:
                pos.value = args
                return None
        if len(args) < len(posi):
            LogHandler.log_argerr(f"Positional argument missing, expected {len(posi)}")
            return None
        for pos in posi:
            pos.value = args.pop(0)

        for arg_left in args:
            LogHandler.log_argerr(f"Unknown argument '{arg_left}'")
            return None

    @staticmethod
    def add_default_arguments() -> None:
        remove_defaults_from_data = True
        _ = VariableFactory.variable(name="conf_source", help="Sets config file source", grp="CLI", env=f"{ConfigHandler.env_slug}_CONFIG", default=ConfigHandler.config_sources[0], remove=remove_defaults_from_data)

        var_usage = VariableFactory.variable(name="cli_usage", help="Shows usage messages",grp="CLI", default=0, remove=remove_defaults_from_data)
        ArgumentFactory.add_argument(var=var_usage, help="Show help message", flg="help",value=1)
        ArgumentFactory.add_argument(var=var_usage, help="Show extended help message", flg="help-extended",value=2)
        ArgumentFactory.add_argument(var=var_usage, help="Show version information", flg="version",value=3)
        ArgumentFactory.add_argument(var=var_usage, help="Show parsable version", flg="version-string",value=4)

        var_loglevel = VariableFactory.variable(name="log_level", help="Sets cli log level", type="string", fmt="choices:info,debug,warning,critical", env=f"{ConfigHandler.env_slug}_LOGLEVEL", cnf="log.level", grp="CLI", default=ConfigHandler.loglevel, remove=remove_defaults_from_data)
        ArgumentFactory.add_argument(var=var_loglevel, help="Set specific log level", flg="loglevel",help_value="<info|debug|warning|critical>")
        ArgumentFactory.add_argument(var=var_loglevel, help="Set verbose log level", flg="verbose,v",value="debug")
        ArgumentFactory.add_argument(var=var_loglevel, help="Set log level to critical only", flg="quiet,q",value="critical")

        _ = VariableFactory.variable(name="log_enabled", help="Enables logging to file", grp="CLI", cnf="log.enabled,log.to_file", type="boolean", default=ConfigHandler.log_enabled, remove=remove_defaults_from_data)
        _ = VariableFactory.variable(name="log_file", help="Sets log file", grp="CLI", cnf="log.file", type="string", default=ConfigHandler.log_file, remove=remove_defaults_from_data)

        _ = VariableFactory.variable(name="lock_enabled", help="Enables locking to file", grp="CLI", cnf="lock.enabled", type="boolean", default=ConfigHandler.lock_enabled, remove=remove_defaults_from_data)
        _ = VariableFactory.variable(name="lock_file", help="Sets lock file", grp="CLI", cnf="lock.file", type="string", default=ConfigHandler.lock_file, remove=remove_defaults_from_data)


    @classmethod
    def parse(cls) -> None:

        cls._parse_sources()

        args_passed = ConfigHandler.arguments
        command, args_list = cls._extract_command(args_passed)
        args_list = cls._expand_shorts(args_list)
        args_list = cls._expand_equals(args_list)
        args_list = cls._parse_arguments(command, args_list)
        args_list = cls._parse_positional(command, args_list)

        VariableFactory.parse_all()
        VariableFactory.check_missing(command)

        # Pass defaults back to ConfigHandler for log init
        ConfigHandler.set_loglevel(VariableFactory.get_variable_value('log_level', True))
        ConfigHandler.set_log_enabled(VariableFactory.get_variable_value('log_enabled', True))
        ConfigHandler.set_log_file(VariableFactory.get_variable_value('log_file', True))
        ConfigHandler.set_lock_enabled(VariableFactory.get_variable_value('lock_enabled', True))
        ConfigHandler.set_lock_file(VariableFactory.get_variable_value('lock_file', True))
        ConfigHandler.show_usage = VariableFactory.get_variable_value('cli_usage', True)
