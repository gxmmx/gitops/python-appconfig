import re

def slugify(string:str, delimiter:str='-', upper:bool=False) -> str:
    pattr = r'[^a-z0-9]+'
    if string:
        string = string.lower()
        string = re.sub(pattr, delimiter, string)
        string = re.sub(re.escape(delimiter) + r'+', delimiter, string)
        string = string.strip(delimiter)
        if upper:
            string = string.upper()
    return string

def is_semver(string:str) -> bool:
    pattr = r'^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$'
    if re.match(pattr, string):
        return True
    return False
