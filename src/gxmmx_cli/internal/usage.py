
from ..internal.handlers import ConfigHandler, ExitHandler
from ..internal.variables import CommandFactory, VariableFactory, ArgumentFactory, PositionalFactory, UsageFactory, Command, Argument, Variable

class UsageHandler:

    @staticmethod
    def parse() -> None:
        if ConfigHandler.show_usage == 1:
            UsageHandler.show_help()
            ExitHandler.exit(0)

        elif ConfigHandler.show_usage == 2:
            UsageHandler.show_extended_help()
            ExitHandler.exit(0)

        elif ConfigHandler.show_usage == 3:
            UsageHandler.show_version()
            ExitHandler.exit(0)

        elif ConfigHandler.show_usage == 4:
            UsageHandler.show_version_string()
            ExitHandler.exit(0)

        elif ConfigHandler.show_usage == 5:
            UsageHandler.show_help()
            ExitHandler.exit(2)

        else:
            cmd = CommandFactory.get_current()
            if cmd is None:
                if CommandFactory.get_children(cmd):
                    UsageHandler.show_help()
                    ExitHandler.exit(0)
            else:
                if not cmd.callable:
                    UsageHandler.show_help()
                    ExitHandler.exit(0)
            return None

    @staticmethod
    def show_version() -> None:
        print(f"\n{ConfigHandler.title} ({ConfigHandler.name})\n\nVersion: {ConfigHandler.version}\n")
        print(f"{ConfigHandler.description}\n")

    @staticmethod
    def show_version_string() -> None:
        print(ConfigHandler.version)

    @staticmethod
    def _show_help_header(cmd:Command|None) -> None:
        if cmd is None:
            UsageHandler.show_version()
        else:
            print(f"\n{ConfigHandler.name} {cmd.name_full}\n\n{cmd.help}\n")

    @staticmethod
    def _show_usage_examples(cmd:Command|None) -> None:
        custom_examples = UsageFactory.get_usages(cmd)
        prefix = ConfigHandler.name
        if cmd is not None:
            prefix = f"{prefix} {cmd.name_full}"
        print("Usage:")
        for usage in custom_examples:
            print(f"  {prefix} {usage.example}")

        variables = VariableFactory._instance_list
        # User options
        usage_string = f"{prefix}"
        for var in variables:
            if var.group != "CLI" and cmd is not None and cmd in var.cmds:
                if var.args:
                    arg_string = '|'.join([a.flag_for_print for a in var.args])
                    if var.default is None:
                        usage_string += f" {arg_string}"
                    else:
                        usage_string += f" [{arg_string}]"
        for var in variables:
            if var.group != "CLI" and cmd is not None and cmd in var.cmds:
                if var.poss:
                    usage_string += " -- "
                    usage_string += ' '.join([p.help_value for p in var.poss])
        print(f"  {usage_string}")
        # Cli options
        usage_string = f"{prefix}"
        for var in variables:
            if var.group == "CLI":
                if var.args:
                    arg_string = '|'.join([a.flag_for_print for a in var.args])
                    usage_string += f" [{arg_string}]"
        print(f"  {usage_string}")
        print()


    @staticmethod
    def _show_help_commands(cmd:Command|None) -> None:
        children = CommandFactory.get_children(cmd)
        if children:
            print("Commands:")
            max_length = max(len(cmd.name_leaf) for cmd in children)
            for cmd_child in children:
                print(f"  {cmd_child.name_leaf:<{max_length + 2}}{cmd_child.help}")
            print()

    @staticmethod
    def _show_help_arguments(cmd:Command|None) -> None:
        args = ArgumentFactory.get_arguments(cmd)
        posi = PositionalFactory.get_positionals(cmd)
        if args:
            group:dict[str,list[Argument]] = {}
            groups:list[str] = []
            ml_args = max([len(i.flags_for_print) for i in args] if args else [0])
            ml_posi = max([len(p.help_value) for p in posi] if posi else [0])
            max_length = max(ml_args,ml_posi)
            for arg in args:
                if arg.var.group is None:
                    group.setdefault('generic', [])
                    group['generic'].append(arg)
                else:
                    if arg.var.group not in groups:
                        groups.append(arg.var.group)
                    group.setdefault(arg.var.group, [])
                    group[arg.var.group].append(arg)

            # Generic options
            if group.get('generic'):
                print("Generic options:")
                for arg in group['generic']:
                    val = ""
                    if arg.value is None:
                        val = " (required)" if arg.var.default is None else f" (def: {arg.var.default_placeholder or arg.var.default})"
                    print(f"  {arg.flags_for_print:<{max_length + 2}}{arg.help}{val}")
                print()
            # Option groups
            for group_name in groups:
                print(f"{group_name} options:")
                for arg in group[group_name]:
                    val = ""
                    if arg.value is None:
                        val = " (required)" if arg.var.default is None else f" (def: {arg.var.default_placeholder or arg.var.default})"
                    print(f"  {arg.flags_for_print:<{max_length + 2}}{arg.help}{val}")
                print()

    @staticmethod
    def _show_help_positional(cmd:Command|None) -> None:
        args = ArgumentFactory.get_arguments(cmd)
        posi = PositionalFactory.get_positionals(cmd)
        if posi:
            print("Positionals:")
            ml_args = max([len(i.flags_for_print) for i in args] if args else [0])
            ml_posi = max([len(p.help_value) for p in posi] if posi else [0])
            max_length = max(ml_args,ml_posi)
            for pos in posi:
                print(f"  {pos.help_value:<{max_length + 2}}{pos.help}")

    @staticmethod
    def _show_help_variables(cmd:Command|None) -> None:
        variables = VariableFactory._instance_list
        group:dict[str,list[Variable]] = {}
        groups:list[str] = []
        for var in variables:
            if var.group is None:
                group.setdefault('generic', [])
                group['generic'].append(var)
            else:
                if var.group not in groups:
                    groups.append(var.group)
                group.setdefault(var.group, [])
                group[var.group].append(var)

        print("Variables:")
        if group.get('generic'):
            print("  Generic vars:")
            for var in group['generic']:
                print(f"    {var.name}")
                print(f"      {var.help}")
                if var.args:
                    print("      args:")
                    for arg in var.args:
                        print(f"        {arg.flags_for_print}")
                if var.poss:
                    print("      args:")
                    for pos in var.poss:
                        print(f"        {pos.help_value}")
                if var.envs:
                    print(f"      envs: {', '.join([env.name for env in var.envs])}")
                if var.cnfs:
                    print(f"      conf: {', '.join([cnf.name for cnf in var.cnfs])}")
                if var.default is not None:
                    print(f"      default: {var.default}")
                else:
                    print(f"      required: True")

        for group_name in groups:
            print(f"  {group_name} vars:")
            for var in group[group_name]:
                print(f"    {var.name}")
                print(f"      {var.help}")
                if var.args:
                    print("      args:")
                    for arg in var.args:
                        print(f"        {arg.flags_for_print}")
                if var.poss:
                    print("      args:")
                    for pos in var.poss:
                        print(f"        {pos.help_value}")
                if var.envs:
                    print(f"      envs: {', '.join([env.name for env in var.envs])}")
                if var.cnfs:
                    print(f"      conf: {', '.join([cnf.name for cnf in var.cnfs])}")
                if var.default is not None:
                    print(f"      default: {var.default}")
                else:
                    print(f"      required: True")

    @staticmethod
    def show_extended_help():
        cmd = CommandFactory.get_current()
        UsageHandler._show_help_header(cmd)
        UsageHandler._show_usage_examples(cmd)
        UsageHandler._show_help_commands(cmd)
        UsageHandler._show_help_variables(cmd)

    @staticmethod
    def show_help():
        cmd = CommandFactory.get_current()
        UsageHandler._show_help_header(cmd)
        UsageHandler._show_usage_examples(cmd)
        UsageHandler._show_help_commands(cmd)
        UsageHandler._show_help_arguments(cmd)
        UsageHandler._show_help_positional(cmd)
