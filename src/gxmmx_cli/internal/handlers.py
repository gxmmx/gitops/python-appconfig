import fcntl
import logging
import os
import sys
import atexit
import typing

from .errors import AppCLIConfigError
from .func import slugify, is_semver

class ConfigHandler:
    # Info
    title = os.path.basename(sys.argv[0])
    name = os.path.basename(sys.argv[0])
    description = f"This is a cli application generated by GxMMx AppCLI. \nTo set this description, call AppCLI().config.description(desc)"
    version = "0.0.0"
    env_slug = None

    # Help & version
    show_usage = 0

    # Arguments
    arguments = sys.argv[1:]

    # Log
    loglevels = ['info', 'debug', 'warning', 'critical']
    loglevel = None

    log_format_file = "%(asctime)s - %(levelname)8s - %(message)s"
    log_format_stdout = "%(message)s"
    log_format_stderr = "%(levelname)s: %(message)s"
    log_format_debug = "%(levelname)s: %(message)s"

    log_enabled = False
    log_file = None

    # Lock
    lock_enabled = False
    lock_file = None

    # Config
    config_sources = []

    @classmethod
    def set_title(cls, title:str) -> None:
        cls.title = title

    @classmethod
    def set_name(cls, name:str) -> None:
        cls.name = name

    @classmethod
    def set_description(cls, description:str) -> None:
        cls.description = description

    @classmethod
    def set_version(cls, version:str) -> None:
        if not is_semver(version):
            raise AppCLIConfigError("Invalid semantic version string")
        cls.version = version

    @classmethod
    def use_arguments(cls, arguments:list) -> None:
        cls.arguments = arguments

    @classmethod
    def set_loglevel(cls, loglevel:str) -> None:
        if loglevel not in cls.loglevels:
            raise AppCLIConfigError(f"Invalid initial loglevel. Must be in '{cls.loglevels}'")
        cls.loglevel = loglevel

    @classmethod
    def set_log_enabled(cls, should:bool) -> None:
        cls.log_enabled = should

    @classmethod
    def set_log_file(cls, log_file:str) -> None:
        if not os.path.exists(os.path.dirname(log_file)):
            return None
        cls.log_file = log_file

    @classmethod
    def set_log_format_file(cls, format:str) -> None:
        cls.log_format_file = format

    @classmethod
    def set_log_format_stdout(cls, format:str) -> None:
        cls.log_format_stdout = format

    @classmethod
    def set_log_format_stderr(cls, format:str) -> None:
        cls.log_format_stderr = format

    @classmethod
    def set_log_format_debug(cls, format:str) -> None:
        cls.log_format_debug = format

    @classmethod
    def set_lock_enabled(cls, should:bool) -> None:
        cls.lock_enabled = should

    @classmethod
    def set_lock_file(cls, lock_file:str) -> None:
        if not os.path.exists(os.path.dirname(lock_file)):
            return None
        cls.lock_file = lock_file

    @classmethod
    def add_config_source(cls, config_file:str) -> None:
        cls.config_sources.append(config_file)

    @classmethod
    def parse(cls) -> None:
        if cls.env_slug is None:
            cls.env_slug = slugify(string=cls.name, delimiter='_', upper=True)

        if cls.log_file is None:
            cls.log_file = f"{os.path.join(os.getcwd(),f'{cls.name}.log')}"

        if cls.lock_file is None:
            cls.lock_file = f"{os.path.join(os.getcwd(),f'{cls.name}.lock')}"

        if len(cls.config_sources) == 0:
            cls.config_sources.append(f"{os.path.join(os.getcwd(),f'{cls.name}.yml')}")

        if cls.loglevel is None:
            cls.loglevel = cls.loglevels[0]


class LogHandler:
    started = False

    pre_debug = []
    pre_info = []
    pre_warning = []
    pre_critical = []
    pre_argerr = []

    format_logfile = None
    format_stdout = None
    format_stderr = None
    format_debug = None

    logger = logging.getLogger(ConfigHandler.name)
    logger.propagate = False

    @classmethod
    def log_debug(cls, msg:str) -> None:
        if cls.started:
            cls.logger.debug(msg)
        else:
            cls.pre_debug.append(f"{msg}")
    @classmethod
    def log_info(cls, msg:str) -> None:
        if cls.started:
            cls.logger.info(msg)
        else:
            cls.pre_info.append(f"{msg}")
    @classmethod
    def log_warning(cls, msg:str) -> None:
        if cls.started:
            cls.logger.warning(msg)
        else:
            cls.pre_warning.append(f"{msg}")
    @classmethod
    def log_critical(cls, msg:str) -> None:
        if cls.started:
            cls.logger.critical(msg)
        else:
            cls.pre_critical.append(f"{msg}")

    @classmethod
    def log_argerr(cls, msg:str) -> None:
        cls.pre_argerr.append(msg)

    @classmethod
    def parse(cls) -> None:
        if cls.started:
            return None
        cls.started = True

        # Skip if usage should be shown
        if ConfigHandler.show_usage > 0:
            return None

        # Level
        cls.logger.setLevel(getattr(logging, ConfigHandler.loglevel.upper(), logging.INFO))

        def init_console() -> None:
            try:
                stdout_format = logging.Formatter(ConfigHandler.log_format_stdout)
                stderr_format = logging.Formatter(ConfigHandler.log_format_stderr)
                debug_format = logging.Formatter(ConfigHandler.log_format_debug)
            except:
                raise AppCLIConfigError('Invalid logging format supplied')

            # Debug handler
            debug_handler = logging.StreamHandler(sys.stdout)
            debug_handler.setFormatter(debug_format)
            debug_handler.setLevel(logging.DEBUG)
            debug_handler.addFilter(lambda record: record.levelno < logging.INFO)
            cls.logger.addHandler(debug_handler)
            # Stdout handler
            stdout_handler = logging.StreamHandler(sys.stdout)
            stdout_handler.setFormatter(stdout_format)
            stdout_handler.setLevel(logging.INFO)
            stdout_handler.addFilter(lambda record: record.levelno == logging.INFO)
            cls.logger.addHandler(stdout_handler)
            # Stderr handler
            stderr_handler = logging.StreamHandler(sys.stderr)
            stderr_handler.setFormatter(stderr_format)
            stderr_handler.setLevel(logging.WARNING)
            cls.logger.addHandler(stderr_handler)

        def init_logfile() -> None:
            if not ConfigHandler.log_enabled:
                return None
            try:
                with open(ConfigHandler.log_file, 'a') as f:
                    pass
            except Exception as e:
                cls.pre_warning.append(f"Logfile can not be written to: '{ConfigHandler.log_file}'")
                ConfigHandler.set_log_enabled(False)
                return None
            try:
                logfile_format = logging.Formatter(ConfigHandler.log_format_debug)
            except:
                raise AppCLIConfigError('Invalid logging format supplied')

            logfile_handler = logging.FileHandler(ConfigHandler.log_file)
            logfile_handler.setFormatter(logfile_format)
            logfile_handler.setLevel(logging.DEBUG)
            cls.logger.addHandler(logfile_handler)
            cls.pre_debug.append(f"Logging to file: '{ConfigHandler.log_file}'")

        def push_pre_log() -> None:
            if cls.pre_argerr:
                for msg in cls.pre_argerr:
                    cls.logger.critical(msg)
                if ConfigHandler.show_usage == 0:
                    ConfigHandler.show_usage = 5
                return None
            cls.logger.debug(f"Starting {ConfigHandler.name}")
            for msg in cls.pre_debug:
                cls.logger.debug(msg)
            for msg in cls.pre_info:
                cls.logger.info(msg)
            for msg in cls.pre_warning:
                cls.logger.warning(msg)
            if cls.pre_critical:
                for msg in cls.pre_critical:
                    cls.logger.critical(msg)
                ExitHandler.exit(2)

        init_console()
        init_logfile()
        push_pre_log()


class LockHandler:
    lock_fd:typing.TextIO|None = None
    lock_file:str|None = None

    @classmethod
    def register(cls) -> None:
        cls.enabled = ConfigHandler.lock_enabled
        cls.lockfile = ConfigHandler.lock_file
        if not cls.enabled:
            return None
        try:
            cls.lock_fd = open(cls.lockfile, 'w')
        except:
            LogHandler.log_critical(f"Could not aquire lockfile '{cls.lockfile}'")
            ExitHandler.exit(1)
        try:
            fcntl.flock(cls.lock_fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except:
            cls.lock_fd.close()
            cls.lock_fd = None
            LogHandler.log_info(f"Execution locked. Another instance of '{ConfigHandler.name}' is already running.")
            LogHandler.log_debug(f"Lockfile: '{cls.lockfile}'")
            ExitHandler.exit(0)
        LogHandler.log_debug(f"Lock aquired: '{cls.lockfile}'")

    @classmethod
    def release(cls) -> None:
        if not cls.enabled:
            return None
        if cls.lock_fd is not None:
            fcntl.flock(cls.lock_fd, fcntl.LOCK_UN)
            cls.lock_fd.close()
            cls.lock_fd = None
            os.remove(cls.lockfile)
            LogHandler.log_debug(f"Lock released: '{cls.lockfile}'")


class ExitHandler:
    @staticmethod
    def _handle_exit() -> None:
        LogHandler.log_debug(f"Exiting {ConfigHandler.name}")
        LockHandler.release()

    @staticmethod
    def register() -> None:
        atexit.register(ExitHandler._handle_exit)

    @staticmethod
    def exit(code:int=0) -> None:
        sys.exit(code)
