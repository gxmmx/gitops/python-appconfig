
from ..internal.errors import AppCLIVariableDefinitionError, AppCLIInternalError

# ------------------------------------------------------------------------------
# Validator Factory
# ------------------------------------------------------------------------------

class ValidatorFactory:
    """
    Creates instance of type to be validated against
    """
    @staticmethod
    def create(name:str, type:str, schema:str|dict|None=None):
        schema_dict = ValidatorFactory._parse_schema(name, schema)
        for cls in Validator.__subclasses__():
            if cls.type_name == type:
                return cls(name, schema_dict)
        raise AppCLIVariableDefinitionError(f"Validation schema not available for type '{type}'")

    @staticmethod
    def _parse_schema(name, schema:str|dict|None):
        if isinstance(schema, dict):
            return schema
        elif schema is None:
            return dict()
        elif isinstance(schema, str):
            if not schema:
                return dict()
            schema_dict = dict()
            pairs = schema.split(';')
            for pair in pairs:
                if ':' in pair:
                    key, value = pair.split(':', 1)
                    if key and value:
                        schema_dict[key.strip()] = value.strip()
                else:
                    raise AppCLIVariableDefinitionError(f"Validation schema for '{name}' contains invalid pair")
        else:
            raise AppCLIVariableDefinitionError(f"Validation schema for '{name}' is invalid, must be string or dict")
        return schema_dict


# ------------------------------------------------------------------------------
# Validator
# ------------------------------------------------------------------------------

class Validator:
    """
    Base class for type validators
    """
    ## Type name.
    #  Must be set in child classes
    type_name = None

    def __init__(self, name:str, schema: dict):
        self.name = name
        self.schema = schema
        self.err = []

    def error(self):
        return self.err[0]

    ## Type specific validation
    #  Must be overwritten in child classes
    def validate(self, value) -> bool:
        raise AppCLIInternalError("Type is missing variable validation function.")


# ------------------------------------------------------------------------------
# String validator
# ------------------------------------------------------------------------------

class StringValidator(Validator):
    """
    Validates variables of type: string
    """
    type_name = "string"

    def validate(self, value:any) -> bool:
        if not isinstance(value, str):
            self.err.append(f"Invalid value for '{self.name}'. Must be of type string.")
            return not bool(self.err)
        if self.schema.get('min'):
            try:
                min_len = int(self.schema.get('min'))
            except:
                raise AppCLIVariableDefinitionError(f"Validation schema for '{self.name}' is invalid. Value 'min' must be integer.")
            if len(value) < min_len:
                self.err.append(f"Invalid value for '{self.name}'. Min length: {min_len}")
        if self.schema.get('max'):
            try:
                max_len = int(self.schema.get('max'))
            except:
                raise AppCLIVariableDefinitionError(f"Validation schema for '{self.name}' is invalid. Value 'max' must be integer.")
            if len(value) > max_len:
                self.err.append(f"Invalid value for '{self.name}'. Max length: {max_len}")
        if self.schema.get('choices'):
            try:
                choices = self.schema.get('choices').split(',')
                if value not in choices:
                    self.err.append(f"Invalid value for '{self.name}'. Choices: {choices}")
            except:
                raise AppCLIVariableDefinitionError(f"Validation schema for '{self.name}' is invalid. Value 'choices' must be comma separated string.")
        return not bool(self.err)


# ------------------------------------------------------------------------------
# Integer validator
# ------------------------------------------------------------------------------

class IntegerValidator(Validator):
    """
    Validates variables of type: integer
    """
    type_name = "integer"

    def validate(self, value:any) -> bool:
        try:
            value = int(value)
        except:
            self.err.append(f"Invalid value for '{self.name}'. Must be of type integer.")
            return not bool(self.err)

        if self.schema.get('min'):
            try:
                min_val = int(self.schema.get('min'))
            except:
                raise AppCLIVariableDefinitionError(f"Validation schema for '{self.name}' is invalid. Value 'min' must be integer.")
            if value < min_val:
                self.err.append(f"Invalid value for '{self.name}'. Min value: {min_val}")
        if self.schema.get('max'):
            try:
                max_val = int(self.schema.get('max'))
            except:
                raise AppCLIVariableDefinitionError(f"Validation schema for '{self.name}' is invalid. Value 'max' must be integer.")
            if value > max_val:
                self.err.append(f"Invalid value for '{self.name}'. Max value: {max_val}")
        if self.schema.get('choices'):
            try:
                choices = str(self.schema.get('choices')).split(',')
                if value not in [int(c) for c in choices]:
                    self.err.append(f"Invalid value for '{self.name}'. Choices: {choices}")
            except:
                raise AppCLIVariableDefinitionError(f"Validation schema for '{self.name}' is invalid. Value 'choices' must be comma separated string.")
        return not bool(self.err)

# ------------------------------------------------------------------------------
# Boolean validator
# ------------------------------------------------------------------------------

class BooleanValidator(Validator):
    """
    Validates variables of type: boolean
    """
    type_name = "boolean"

    def validate(self, value) -> bool:
        if not isinstance(value, bool):
            self.err.append(f"Invalid value for '{self.name}'. Must be of type boolean.")
        return not bool(self.err)
