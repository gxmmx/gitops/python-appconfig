
class AppCLIError(Exception):
    def __init__(self, message):
        super().__init__(f"AppCLI error: {message}")

class AppCLIRuntimeError(Exception):
    def __init__(self, message):
        super().__init__(f"AppCLI Runtime error: {message}")

class AppCLIConfigError(Exception):
    def __init__(self, message):
        super().__init__(f"AppCLI Config error: {message}")

class AppCLIVariableDefinitionError(Exception):
    def __init__(self, message):
        super().__init__(f"AppCLI Variable definition error: {message}")

class AppCLIArgumentError(Exception):
    def __init__(self, message):
        super().__init__(f"AppCLI Argument error: {message}")

class AppCLIInternalError(Exception):
    def __init__(self, message):
        super().__init__(f"AppCLI Internal error: {message}")
