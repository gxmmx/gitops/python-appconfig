import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli import AppCLI

def test_start() -> None:
    cli = AppCLI()
    cli.config.use_arguments(['mycmd', "-a", "bar1", "bar2"])
    assert cli.start() is None
    # Run again to test idempotency
    assert cli.start() is None
