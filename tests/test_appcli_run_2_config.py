import pytest
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli import AppCLI
from gxmmx_cli.internal.handlers import ConfigHandler
from gxmmx_cli.internal.variables import VariableFactory, CommandFactory
from gxmmx_cli.interfaces.state import StateInterface
from gxmmx_cli.internal.errors import AppCLIConfigError

def test_general_config() -> None:
    cli = AppCLI()
    cli.config.use_arguments([])
    cli.config.title("Test Title")
    cli.config.name("testname")
    cli.config.description("testdesc")
    
    with pytest.raises(AppCLIConfigError):
        cli.config.version("0.1.1err")
    cli.config.version("0.1.1")

    assert ConfigHandler.title == "Test Title"
    assert ConfigHandler.name == "testname"
    assert ConfigHandler.description == "testdesc"
    assert ConfigHandler.version == "0.1.1"

def test_logging_config() -> None:
    cli = AppCLI()
    with pytest.raises(AppCLIConfigError):
        cli.config.loglevel("nonexistent")
    cli.config.loglevel("info")
    cli.config.log_enable(False)
    logfile = f"{os.getcwd()}/testname.log"
    cli.config.log_file("/some/non/existing/dir")
    cli.config.log_file(logfile)
    cli.config.log_format_debug('%(message)s')
    cli.config.log_format_stdout('%(message)s')
    cli.config.log_format_stderr('%(message)s')
    cli.config.log_format_file('%(message)s')
    assert ConfigHandler.loglevel == "info"
    assert ConfigHandler.log_enabled == False
    assert ConfigHandler.log_file == logfile
    assert ConfigHandler.log_format_debug == '%(message)s'
    assert ConfigHandler.log_format_stdout == '%(message)s'
    assert ConfigHandler.log_format_stderr == '%(message)s'
    assert ConfigHandler.log_format_file == '%(message)s'

def test_locking_config() -> None:
    cli = AppCLI()
    cli.config.lock_enable(False)
    lockfile = f"{os.getcwd()}/testname.lock"
    cli.config.lock_file("/some/non/existing/dir")
    cli.config.lock_file(lockfile)
    assert ConfigHandler.lock_enabled == False
    assert ConfigHandler.lock_file == lockfile

def test_config_source() -> None:
    cli = AppCLI()
    cli.config.add_config_file("/tmp/testname.conf")
    assert "/tmp/testname.conf" in ConfigHandler.config_sources

def test_arguments() -> None:
    cli = AppCLI()
    cli.config.add_command("mycmd", "mycmd help")
    cli.config.add_usage("--custom usage message", cmd="mycmd")
    cli.config.add_variable("myvar", "myvar help", default="myvarvalue")
    cli.config.add_argument("argfoo", "argfoo,a", "argfoo help", value="setvalue", help_value=None, type="string", fmt="min:1", env="TESTAPP_ARGFOO", cnf="app.argfoo", default="unsetvalue", grp="My args")
    cli.config.add_argument("argbar", "argbar,b", "argbar help", value=None, help_value="<mybarval>", type="string", fmt="min:1", env="TESTAPP_ARGBAR", cnf="app.argbar", default="defbarval", grp="My args", cmd="mycmd")
    cli.config.add_positional("mypos", "mypos help", recurring=True, help_value="<mypos>...", cmd="mycmd")

    assert "mycmd" in CommandFactory._instance_dict
    assert "myvar" in VariableFactory._instance_dict
    assert "argfoo" in VariableFactory._instance_dict
    assert "argbar" in VariableFactory._instance_dict
    assert "mypos" in VariableFactory._instance_dict
    assert StateInterface.started == False
