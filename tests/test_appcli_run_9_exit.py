import pytest
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli import AppCLI

def test_exit() -> None:
    cli = AppCLI()
    with pytest.raises(SystemExit):
        assert cli.exit(0) is None
