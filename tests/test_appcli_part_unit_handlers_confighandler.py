import pytest
import importlib
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli.internal.handlers import ConfigHandler

@pytest.fixture(autouse=True)
def reload_module():
    from gxmmx_cli.internal.handlers import ConfigHandler
    importlib.reload(ConfigHandler)
    yield

def test_config_handler() -> None:
    h = ConfigHandler
    
