import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli.internal.func import slugify, is_semver

def test_slufigy() -> None:
    string = "This Is My String"
    slug = slugify(string,delimiter='.',upper=False)
    assert slug == "this.is.my.string"

def test_is_semver() -> None:
    valid_version = "1.0.1"
    invalid_version = "1.0.1a"
    assert is_semver(valid_version)
    assert not is_semver(invalid_version)
