import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli import AppCLI

def test_data() -> None:
    cli = AppCLI()
    cli.log.debug("debug")
    cli.log.info("info")
    cli.log.warning("warning")
    cli.log.critical("critical")
