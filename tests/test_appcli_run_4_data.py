import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli import AppCLI

def test_data() -> None:
    cli = AppCLI()
    variables = cli.data.variables()
    foo = variables.get("argfoo")
    assert foo == "setvalue"
    assert cli.data.command() == "mycmd"
    assert cli.data.variable("myvar") == "myvarvalue"
