import pytest
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli.internal.errors import *

def test_generic_error() -> None:
    with pytest.raises(AppCLIError):
        raise AppCLIError("appcli generic error")

def test_generic_error() -> None:
    with pytest.raises(AppCLIRuntimeError):
        raise AppCLIRuntimeError("appcli runtime error")
    with pytest.raises(AppCLIConfigError):
        raise AppCLIConfigError("appcli config error")
    with pytest.raises(AppCLIArgumentError):
        raise AppCLIArgumentError("appcli arg error")
    with pytest.raises(AppCLIVariableDefinitionError):
        raise AppCLIVariableDefinitionError("appcli variable definition error")
    with pytest.raises(AppCLIInternalError):
        raise AppCLIInternalError("appcli internal error")
