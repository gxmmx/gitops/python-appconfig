import sys
import os
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from gxmmx_cli import AppCLI

def main():
    cli = AppCLI()
    cli.config.use_arguments([])
    cli.config.title("Test Title")
    cli.config.name("testname")
    cli.config.description("testdesc")
    cli.config.version("0.1.1")
    cli.config.loglevel("info")
    cli.config.log_enable(False)
    logfile = f"{os.getcwd()}/testname.log"
    cli.config.log_file(logfile)
    cli.config.log_format_debug('%(message)s')
    cli.config.log_format_stdout('%(message)s')
    cli.config.log_format_stderr('%(message)s')
    cli.config.log_format_file('%(message)s')
    cli.config.lock_enable(False)
    lockfile = f"{os.getcwd()}/testname.lock"
    cli.config.lock_file(lockfile)
    cli.config.add_config_file("/tmp/testname.conf")
    cli.config.add_command("mycmd", "mycmd help")
    cli.config.add_usage("--custom usage message", cmd="mycmd")
    cli.config.add_variable("myvar", "myvar help", default="myvarvalue")
    cli.config.add_argument("argfoo", "argfoo,a", "argfoo help", value="setvalue", help_value=None, type="string", fmt="min:1", env="TESTAPP_ARGFOO", cnf="app.argfoo", default="unsetvalue", grp="My args")
    cli.config.add_argument("argbar", "argbar,b", "argbar help", value=None, help_value="<mybarval>", type="string", fmt="min:1", env="TESTAPP_ARGBAR", cnf="app.argbar", default="defbarval", grp="My args", cmd="mycmd")
    cli.config.add_positional("mypos", "mypos help", recurring=True, help_value="<mypos>...", cmd="mycmd")

    cli.start()

if __name__ == '__main__':
    main()
